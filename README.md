# Try-out
Machine learning algorithms in Python

Sources:
  
  https://www.kaggle.com/vinchinzu/dc-metro-crime-data
  
  https://www.kaggle.com/uciml/student-alcohol-consumption
  
  https://archive.ics.uci.edu/ml/datasets/Wine
  
  https://www.kaggle.com/miroslavsabo/young-people-survey
